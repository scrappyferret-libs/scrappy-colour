--- Required libraries.

--- Localised functions.

-- Localised values.

--- Class creation.
local library = {}

--- Initialises this Scrappy library.
-- @param params The params for the initialisation.
function library:init( params )

	-- Store out the params, if any
	self._params = params or {}

	-- Table to store palettes
	self._palettes = {}

	-- Table to store colours
	self._colours = {}

end

--- Register a named colour to the system.
-- @param name The name of the colour.
-- @param values Indexed Table containing red, green, blue values. Alpha value optional, defaults to 1.
function library:register( name, values )
	if type( values ) == "string" then
		values = self:hexToRGB( values )
	end
	self._colours[ name ] = values
end

--- Gets a named colour from the system.
-- @param name The name of the colour.
-- @param palette The name of the palette. Optional, defaults to nil.
-- @param asTable True if you want the colours back as a table rather than multiple return values. Optional, defaults to false.
-- @return The colour values, or as a table if desired.
function library:get( name, palette, asTable )

	local colour

	if palette then
		colour = self:get( self._palettes[ palette ][ name ], nil, true )
	else
		colour = self._colours[ name ]
	end

	if asTable and colour then
		colour[ 4 ] = colour[ 4 ] or 1
		return colour
	elseif colour then
		return colour[ 1 ], colour[ 2 ], colour[ 3 ], colour[ 4 ] or 1
	end

end

--- Gets all named colours from the system.
-- @param palette The name of the palette if you'd just like those colours. Optional, defaults to nil.
-- @return The colour names.
function library:list( palette )
	if palette then
		return self._palettes[ palette ] or {}
	else
		return self._colours or {}
	end
end

--- Creates a colour palette.
-- @param name The name for the palette.
-- @return The name of the palette.
function library:createPalette( name )
	self._palettes[ name ] = {}
	return name
end

--- Adds a named colour to a palette.
-- @param palette The name for the palette.
-- @param name The palette-specific name of the colour.
-- @param colour The name of the colour that was originally added to the system.
function library:addToPalette( palette, name, colour )
	self._palettes[ palette ][ name ] = colour
end

--- Converts a hex string colour to r, g, b values.
-- @param hex The hex string.
-- @return An indexed table containing the r, g, and b values.
function library:hexToRGB( hex )
	if hex and hex[ "gsub" ] then
    	hex = hex:gsub( "#","" )
    	return { tonumber("0x" .. hex:sub( 1, 2 ) ) / 255, tonumber( "0x".. hex:sub( 3, 4 ) ) / 255, tonumber( "0x" .. hex:sub( 5, 6 ) ) / 255 }
	else
		return nil
	end
end

--- Converts a colour to grayscale.
-- @param r The R value.
-- @param g The G value.
-- @param b The B value.
-- @return The converted r, g, and b values.
function library:rgbToGrayscale( r, g, b )
	local bw = ( r + g + b ) / 3
	return bw, bw, bw
end

--- Converts a colour to sepia.
-- @param r The R value.
-- @param g The G value.
-- @param b The B value.
-- @return The converted r, g, and b values.
function library:rgbToSepia( r, g, b )

	local tr = 0.393 * r + 0.769 * g + 0.189 * b
	local tg = 0.349 * r + 0.686 * g + 0.168 * b
	local tb = 0.272 * r + 0.534 * g + 0.131 * b

	if tr > 1 then r = 1 else r = tr end
	if tg > 1 then g = 1 else g = tg end
	if tb > 1 then b = 1 else b = tb end

	return tr, tg, tb

end

--- Destroys this library object.
function library:destroy()
	self._colours = nil
	self._palettes = nil
end

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

-- If we don't have a Scrappy Colour library
if not Scrappy.Colour then

	-- Then store the library out
	Scrappy.Colour = library

end

-- Return the new library
return library
